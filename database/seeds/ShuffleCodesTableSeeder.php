<?php

use App\Traits\DigitalCodeTrait;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ShuffleCodesTableSeeder extends Seeder
{
    use DigitalCodeTrait;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->shuffleCodes() as $code) {
            DB::table('shuffle_codes')->insert([
                'code' => $code,
            ]);
        }
    }
}
