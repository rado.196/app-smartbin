<?php

use App\Models\Transaction;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $states = [
                Transaction::STATE_WAITING,
                Transaction::STATE_COMPLETE,
                Transaction::STATE_CANCELED,
            ];

            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('history_id')->unsigned()->nullable();
            $table->string('token')->unique();
            $table->enum('state', $states);

            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('history_id')->references('id')->on('histories');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
