<?php

namespace App\Providers;

use App\Events\CodeChangeEvent;
use App\Events\ResetPasswordEvent;
use App\Events\UserRegisterEvent;
use App\Listeners\CodeChangeListener;
use App\Listeners\ResetPasswordListener;
use App\Listeners\UserRegisterListener;
use Illuminate\Support\Facades\Event;
use Illuminate\Auth\Events\Registered as RegisteredEvent;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification as RegisteredListener;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        RegisteredEvent::class => [
            RegisteredListener::class,
        ],

        UserRegisterEvent::class => [
            UserRegisterListener::class,
        ],

        CodeChangeEvent::class => [
            CodeChangeListener::class,
        ],

        ResetPasswordEvent::class => [
            ResetPasswordListener::class,
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
