<?php

namespace App\Listeners;

use App\Events\UserRegisterEvent;
use App\Traits\NotifyBySmsOrEmailTrait;

class UserRegisterListener
{
    use NotifyBySmsOrEmailTrait;

    public function handle(UserRegisterEvent $event)
    {
        $user = $event->getRegisteredUser();
        $message = 'Ձեր SmartBin կոդը՝ ' . $user->code;

        $this->eventSend($event->getPrior(), $user, $message);
    }
}
