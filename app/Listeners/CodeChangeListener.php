<?php

namespace App\Listeners;

use App\Events\CodeChangeEvent;
use App\Traits\NotifyBySmsOrEmailTrait;

class CodeChangeListener
{
    use NotifyBySmsOrEmailTrait;

    public function handle(CodeChangeEvent $event)
    {
        $user = $event->getUnverifiedUser();
        $message = 'Ձեր ակտիվացման կոդը՝ ' . $user->code;

        $this->eventSend($event->getPrior(), $user, $message);
    }
}
