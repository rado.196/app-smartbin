<?php

namespace App\Traits;

use Illuminate\Support\Facades\DB;

trait DigitalCodeTrait
{
    private static function getMaxCodeValue($count)
    {
        return pow(10, $count) - 1;
    }

    private function shuffleCodes()
    {
        $codes = range(1, self::getMaxCodeValue(4));
        shuffle($codes);

        foreach ($codes as & $code) {
            $code = str_pad($code, 4, '0', STR_PAD_LEFT);
        }

        return $codes;
    }

    private function getNextFreeCode()
    {
        $shuffleCode = DB::table('shuffle_codes')
            ->where('used', false)
            ->first();

        $shuffleCode = $shuffleCode->code;
        DB::table('shuffle_codes')
            ->where('used', false)
            ->limit(1)
            ->update([
                'used' => true,
            ]);

        return $shuffleCode;
    }

    private function changeCode($oldCode)
    {
        $newCode = $this->getNextFreeCode();
        DB::table('shuffle_codes')
            ->where('code', $oldCode)
            ->update([
                'used' => false,
            ]);

        return $newCode;
    }

    private function generateRandomCode()
    {
        $code = rand(1, self::getMaxCodeValue(4));
        $code = str_pad($code, 4, '0', STR_PAD_LEFT);

        return $code;
    }
}
