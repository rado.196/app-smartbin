<?php

namespace App\Traits;

use App\Models\User;
use Illuminate\Mail\Message;
use Illuminate\Support\Facades\Mail;
use Throwable;
use Twilio\Rest\Client;

trait NotifyBySmsOrEmailTrait
{
    private function notifyByEmailToUser(User $user, $subject, $view, array $viewData = [])
    {
        $userInfo = [
            'email' => $user->email,
            'name' => $user->first_name . ' ' . $user->last_name,
        ];

        $this->notifyByEmailToMail($userInfo, $subject, $view, $viewData);
    }

    private function notifyByEmailToMail($user, $subject, $view, array $viewData = [])
    {
        Mail::send($view, $viewData, function (Message $mailMessage) use ($user, $subject) {
            $mailMessage->subject($subject)
                ->from('no-reply@beeoncode.com', env('APP_NAME', 'BeeOnCode Training'))
                ->to($user['email'], $user['name']);
        });
    }

    private function notifyBySmsToUser(User $user, $message)
    {
        $this->notifyBySmsToPhone($user->phone, $message);
    }

    private function notifyBySmsToPhone($phone, $message)
    {
        $accountSid = config('app.twilio.account_sid');
        $authToken = config('app.twilio.auth_token');
        $phoneNumber = config('app.twilio.phone_number');

        $client = new Client($accountSid, $authToken);
        $client->messages->create($phone, [
            'from' => $phoneNumber,
            'body' => $message,
        ]);
    }

    private function getUserProvider($user)
    {
        $user = User::find($user->id);
        return $user->provider == User::PROVIDER_EMAIL
            ? 'email'
            : 'phone';
    }

    private function eventSend($prior, User & $user, & $message)
    {
        $sendSms = function (User & $user, & $message) {
            $this->notifyBySmsToUser($user, $message);

            $user->provider = User::PROVIDER_PHONE;
            $user->save();
        };

        $sendMail = function (User & $user, & $message) {
            $this->notifyByEmailToUser($user, 'SmartBin', 'email', [
                'body' => $message,
            ]);

            $user->provider = User::PROVIDER_EMAIL;
            $user->save();
        };

        if ($prior == User::PROVIDER_EMAIL) {
            try {
                $sendMail($user, $message);
            } catch (Throwable $t) {
                echo $t->getMessage();
                $sendSms($user, $message);
            }
        } else {
            try {
                $sendSms($user, $message);
            } catch (Throwable $t) {
                echo $t->getMessage();
                $sendMail($user, $message);
            }
        }
    }
}
