<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class FrontController extends Controller
{
    public function googleMap()
    {
        return response()->json([
            'apiKey' => env('GOOGLE_MAP_API_KEY'),
            'initialZoom' => env('GOOGLE_MAP_INITIAL_ZOOM'),
            'coordinate' => [
                'latitude' => env('GOOGLE_MAP_LATITUDE'),
                'longitude' => env('GOOGLE_MAP_LONGITUDE'),
            ],
        ]);
    }

    public function index()
    {
        return view('front.home');
    }
}
