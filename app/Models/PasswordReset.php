<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PasswordReset extends Model
{
    public $timestamps = false;

    public static function boot()
    {
        parent::boot();
        
        static::creating(function (self $model) {
            $model->created_at = $model->freshTimestamp();
        });
    }

    protected $table = 'password_resets';

    protected $fillable = [
        'user_id',
        'code',
    ];

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }
}
