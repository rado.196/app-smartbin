<?php

namespace App\Rules;

use App\Models\User;
use Illuminate\Contracts\Validation\Rule;

class EmailRule implements Rule
{
    private $isValid = true;
    private $isFree = true;

    public function passes($attribute, $value)
    {
        $this->checkIsValid($value);
        $this->checkIsFree($value);

        return $this->isValid && $this->isFree;
    }

    public function message()
    {
        if (!$this->isValid) {
            return 'Անվավեր էլ-հասցե:';
        }

        if (!$this->isFree) {
            return 'Էլ-հասցեն արդեն զբաղված է:';
        }
    }

    private function checkIsValid($value)
    {
        $this->isValid = filter_var($value, FILTER_VALIDATE_EMAIL);
    }

    private function checkIsFree($value)
    {
        $this->isFree = !User::where('email', $value)->exists();
    }
}
