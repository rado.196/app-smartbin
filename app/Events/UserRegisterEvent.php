<?php

namespace App\Events;

use App\Models\User;
use Illuminate\Queue\SerializesModels;

class UserRegisterEvent
{
    use SerializesModels;

    private $user;
    private $prior;

    public function __construct(User $user)
    {
        $this->user = $user;
        $this->prior = User::PROVIDER_PHONE;
    }

    public function getRegisteredUser()
    {
        return $this->user;
    }

    public function getPrior()
    {
        return $this->prior;
    }
}
