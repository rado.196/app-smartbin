<?php

use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/google-map', 'FrontController@googleMap');

Route::group(['prefix' => '/auth'], function () {
    Route::post('/register', 'AuthController@register');
    Route::post('/verify', 'AuthController@verify');

    Route::post('/login', 'AuthController@login');
    Route::group(['middleware' => 'auth:api'], function () {
        Route::post('/logout', 'AuthController@logout');
    });

    Route::group(['prefix' => '/password'], function () {
        Route::group(['prefix' => '/reset'], function () {
            Route::post('/email', 'AuthController@forgot');
            Route::post('/verify', 'AuthController@forgotVerify');
            Route::post('/reset', 'AuthController@reset');
        });
    });

    Route::group(['prefix' => '/update', 'middleware' => ['jwt', 'auth:api']], function () {
        Route::post('/profile', 'AuthController@updateProfile');
        Route::post('/password', 'AuthController@updatePassword');
    });

    Route::post('/logged-user', 'AuthController@loggedUser');
});

Route::group(['prefix' => '/user-data'], function () {
    Route::group(['middleware' => ['jwt', 'auth:api']], function () {
        Route::post('/user-registered-date', 'DataController@registeredDate');
        Route::post('/total', 'DataController@total');

        Route::group(['prefix' => '/filter'], function () {
            Route::post('/count', 'DataController@filterCount');
            Route::post('/load', 'DataController@filterLoad');
        });
    });

    Route::post('/top-three', 'DataController@topThree');

    Route::group(['prefix' => '/insert-data', 'middleware' => 'cors'], function () {
        Route::post('/generate-token', 'InsertController@generateToken');
        Route::post('/store-user-data', 'InsertController@storeData');
    });
});
