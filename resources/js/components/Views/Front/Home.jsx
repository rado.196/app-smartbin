import axios from "axios";
import React, { Component } from "react";
import { Link } from "react-router-dom";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Container, Row, Col } from "reactstrap";
import Carousel from "nuka-carousel";

import Routes from "../../Routes";
import Helper from "../../Helpers";

class Home extends Component {
    constructor(props, context) {
        super(props, context);

        this.state = {
            topThree: [],
        };

        this.loadTopThree = this.loadTopThree.bind(this);

        Helper.pageTitle("Գլխավոր էջ");
    }

    componentDidMount() {
        this.loadTopThree();
        Helper.pageTitle("Գլխավոր էջ");
    }

    componentWillUpdate() {
        Helper.pageTitle("Գլխավոր էջ");
    }

    loadTopThree() {
        axios.post("/api/user-data/top-three")
            .then((response) => {
                this.setState({
                    topThree: response.data.topThree,
                });
            });
    }

    render() {
        const carouselProps = {
            disableKeyboardControls: true,
            autoplay: true,
            autoplayInterval: 4000,
            renderCenterLeftControls: ({ previousSlide }) => (
                <i className="fas fa-arrow-left" onClick={previousSlide} />
            ),
            renderCenterRightControls: ({ nextSlide }) => (
                <i className="fas fa-arrow-right" onClick={nextSlide} />
            ),
        };

        return (
            <div>
                <Carousel {...carouselProps}>
                    <div className="site-blocks-cover sbc-1">
                        <Container>
                            <Row className="align-items-center justify-content-center text-center">
                                <Col md={10} xs={6}>
                                    <div className="h1 sbc-content">Քայլ 1: Գրանցվել մեր կայքում</div>
                                </Col>
                            </Row>
                        </Container>
                    </div>
                    <div className="site-blocks-cover sbc-2">
                        <Container>
                            <Row className="align-items-center justify-content-center text-center">
                                <Col md={10} xs={6}>
                                    <div className="h1 sbc-content">Քայլ 2: Ստանալ կոդ</div>
                                </Col>
                            </Row>
                        </Container>
                    </div>
                </Carousel>

                <div className="py-5 bg-success">
                    <Container>
                        <Row className="align-items-center">
                            <Col lg={12}>
                                <div className="d-flex popular-domain-extension">
                                    {this.state.topThree.map((value, i) => (
                                        <div key={i} className={`pL-20 ${i == this.state.topThree.length - 1 ? "" : "pR-20 border-right"}`}>
                                            <strong className="d-block text-white h2">Օգտատեր {i + 1}</strong>
                                            <span className="text-white h6">{value} միավոր</span>
                                        </div>
                                    ))}
                                </div>
                            </Col>
                        </Row>
                    </Container>
                </div>
                <div className="site-blocks-cover no-cover sbc-3">
                    <Container>
                        <Row className="align-items-center text-left">
                            <Col lg={8}>
                                <h1 className="mb-2 sbc-content">Սիրելի Ժողովուրդ․․․</h1>
                                <br />
                                <p className="mb-5 sbc-content">{/*ԹՈՒՂԹԸ ԱՂԲ ՉԻ՛, */}ՊԼԱՍՏԻԿԸ ԱՂԲ ՉԻ՛, ԱՊԱԿԻՆ ԱՂԲ ՉԻ՛</p>
                                {!this.props.isLogged && <span>
                                    <h1 className="mb-2 sbc-content">․․․ և ուրեմն</h1>
                                    <p>
                                        <br />
                                        <br />
                                        <Link to={Routes.toUrl("auth_register")} className="btn btn-success py-3 px-5 rounded-0">Գրանցվել Հիմա</Link>
                                    </p>
                                </span>}
                            </Col>
                        </Row>
                    </Container>
                </div>
            </div>
        );
    }
}

Home.propTypes = {
    dispatch: PropTypes.func.isRequired,
    isLogged: PropTypes.bool.isRequired,
};

export default connect((state) => {
    return {
        isLogged: state.AuthReducer.isLogged,
    };
})(Home);
