import React, { Component } from "react";
import { Container, Row, Col } from "reactstrap";
import axios from "axios";

import Helper from "../../Helpers";

export default class Home extends Component {
    constructor(props, context) {
        super(props, context);

        this.state = {
            loading: true,
        };

        this.initGoogleMap();

        Helper.pageTitle("Հետադարձ կապ");
    }

    componentDidMount() {
        Helper.pageTitle("Հետադարձ կապ");
    }

    componentWillUpdate() {
        Helper.pageTitle("Հետադարձ կապ");
    }

    initGoogleMap() {
        axios.post("/api/google-map")
            .then((response) => response.data)
            .then(({ apiKey, initialZoom, coordinate }) => {
                this.setState({
                    loading: false,
                });

                const initMap = () => {
                    const coords = {
                        lat: parseFloat(coordinate.latitude),
                        lng: parseFloat(coordinate.longitude),
                    };

                    const mapElement = document.getElementById("google-map-area");
                    const googleMap = new google.maps.Map(mapElement, {
                        zoom: parseInt(initialZoom),
                        center: coords,
                        fullscreenControl: true,
                        zoomControl: true,
                        mapTypeControl: false,
                        scaleControl: false,
                        streetViewControl: false,
                        rotateControl: false,
                    });

                    new google.maps.Marker({
                        position: coords,
                        map: googleMap,
                    });
                };

                if (typeof google != "undefined") {
                    initMap();
                } else {
                    this.gMapScript = document.createElement("script");
                    this.waitForScriptLoad(initMap);

                    this.gMapScript.setAttribute("src", `https://maps.googleapis.com/maps/api/js?key=${apiKey}&language=hy&region=AM`);
                    document.body.appendChild(this.gMapScript);
                }
            });
    }

    waitForScriptLoad(callback) {
        const googleMapScriptLoaded = () => {
            document.body.removeChild(this.gMapScript);
            callback();
        };

        if (this.gMapScript.readyState) {
            this.gMapScript.onreadystatechange = () => {
                if (["loaded", "complete"].indexOf(this.gMapScript.readyState) !== -1) {
                    this.gMapScript.onreadystatechange = null;
                    googleMapScriptLoaded();
                }
            };
        } else {
            this.gMapScript.onload = googleMapScriptLoaded;
        }
    }

    render() {
        return (
            <div className="site-section bg-light">
                <Container>
                    <Row>
                        <Col md={12} lg={8}>
                            <div className="p-30 bg-white mB-20 box-shadowed">
                                <h3 className="h5 text-black mB-50">Քարտեզ</h3>

                                {this.state.loading ? (<div>
                                    <div id="google-map-load">
                                        Քարտեզը բեռնվում է ․․․
                                    </div>
                                </div>) : (<div>
                                    <div id="google-map-area" />
                                </div>)}
                            </div>
                        </Col>
                        <Col lg={4}>
                            <div className="p-30 mb-3 bg-white box-shadowed">
                                <h3 className="h5 text-black mB-50">Կոնտակտային Տվյալներ</h3>
                                <p className="mb-0 font-weight-bold">Հասցե</p>
                                <p className="mb-4">
                                    ՀՀ Արարատի մարզ ք. Արարատ<br />
                                    Իսահակյան փ. թիվ 3 մանկապարտեզի մասնաշենք
                                </p>
                                <p className="mb-0 font-weight-bold">Հեռ․</p>
                                <p className="mb-4"><a href="tel:+37441886600">+37441886600</a></p>
                                <p className="mb-0 font-weight-bold">Email</p>
                                <p className="mb-0"><a href="mailto:araratinfotun@gmail.com">araratinfotun@gmail.com</a></p>
                            </div>
                        </Col>
                    </Row>
                </Container>
            </div>
        );
    }
}
