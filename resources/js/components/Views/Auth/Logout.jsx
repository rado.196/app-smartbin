import React, { Component } from "react";
import { Redirect, Link } from "react-router-dom";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Container, Row, Col, FormGroup, Label, Input, Button } from "reactstrap";

import * as service from "../../Service/AuthService";
import Route from "../../Routes";

class Logout extends Component {
    constructor(props, context) {
        super(props, context);
    }

    componentDidMount() {
        service.exec(this, null, service.logout);
    }

    render() {
        if (!this.props.isLogged) {
            return <Redirect to={Route.toUrl("auth_login")} />
        }

        return (
            <div className="site-section bg-light">
                <Container>
                    <Row>
                        <Col lg={4} className="offset-lg-4">
                            <div className="p-25 bg-white box-shadowed">
                                <FormGroup>
                                    <ul className="auth-error-message" id="auth-error-message" />
                                    <ul className="auth-success-message" id="auth-success-message" />
                                </FormGroup>

                                <div className="loading-area" id="loading-area" />
                            </div>
                        </Col>
                    </Row>
                </Container>
            </div>
        );
    }
}

Logout.propTypes = {
    dispatch: PropTypes.func.isRequired,
    isLogged: PropTypes.bool.isRequired,
};

export default connect((state) => {
    return {
        isLogged: state.AuthReducer.isLogged,
    };
})(Logout);
