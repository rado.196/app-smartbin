import React, { Component } from "react";
import { Redirect, Link } from "react-router-dom";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Container, Row, Col, FormGroup, Label, Input, Button } from "reactstrap";
import InputMask from "react-input-mask";
import reactTriggerChange from "react-trigger-change";

import * as service from "../../Service/AuthService";
import Route from "../../Routes";

class Register extends Component {
    constructor(props, context) {
        super(props, context);

        this.state = {
            registered: false,
            firstNameData: "",
            firstNameValid: false,
            lastNameData: "",
            lastNameValid: false,
            addressData: "",
            addressValid: false,
            phoneData: "+374",
            phoneValid: false,
            emailData: "",
            emailValid: true,
            passwordData: "",
            passwordValid: false,
            confirmData: "",
            confirmValid: false,
        };

        this.validateFirstName = this.validateFirstName.bind(this);
        this.validateLastName = this.validateLastName.bind(this);
        this.validateAddress = this.validateAddress.bind(this);
        this.validatePhone = this.validatePhone.bind(this);
        this.validateEmail = this.validateEmail.bind(this);
        this.validatePassword = this.validatePassword.bind(this);
        this.validateConfirm = this.validateConfirm.bind(this);
        this.formSubmitRegister = this.formSubmitRegister.bind(this);
    }

    validateFirstName(event) {
        let element = event.target;
        if (!service.validFirstName(element)) {
            this.setState({
                firstNameData: element.value,
                firstNameValid: false
            });
        } else {
            this.setState({
                firstNameData: element.value,
                firstNameValid: true
            });
        }
    }

    validateLastName(event) {
        let element = event.target;
        if (!service.validLastName(element)) {
            this.setState({
                lastNameData: element.value,
                lastNameValid: false
            });
        } else {
            this.setState({
                lastNameData: element.value,
                lastNameValid: true
            });
        }
    }

    validateAddress(event) {
        let element = event.target;
        if (!service.validAddress(element)) {
            this.setState({
                addressData: element.value,
                addressValid: false
            });
        } else {
            this.setState({
                addressData: element.value,
                addressValid: "" != element.value
            });
        }
    }

    validatePhone(event) {
        let element = event.target;
        if (!service.validPhone(element)) {
            this.setState({
                phoneData: element.value,
                phoneValid: false
            });
        } else {
            this.setState({
                phoneData: element.value,
                phoneValid: "" != element.value
            });
        }
    }

    validateEmail(event) {
        let element = event.target;
        if (!service.validEmail(element)) {
            this.setState({
                emailData: element.value,
                emailValid: false
            });
        } else {
            this.setState({
                emailData: element.value,
                emailValid: "" != element.value
            });
        }
    }

    validatePassword(event) {
        let element = event.target;
        if (!service.validPassword(element)) {
            this.setState({
                passwordData: element.value,
                passwordValid: false
            });
        } else {
            this.setState({
                passwordData: element.value,
                passwordValid: "" != element.value
            });
        }

        const confirm = document.querySelector("input#confirm");
        reactTriggerChange(confirm);
    }

    validateConfirm(event) {
        const password = document.querySelector("input#password");

        let element = event.target;
        if (!service.validConfirm(element, password.value)) {
            this.setState({
                confirmData: element.value,
                confirmValid: false
            });
        } else {
            this.setState({
                confirmData: element.value,
                confirmValid: "" != element.value
            });
        }
    }

    formSubmitRegister(event) {
        event.preventDefault();

        const invalid = !this.state.firstNameValid ||
            !this.state.lastNameValid ||
            !this.state.addressValid ||
            !this.state.phoneValid ||
            !this.state.passwordValid ||
            !this.state.confirmValid;

        if (invalid) {
            service.showAllErrors(event.target);
            return;
        }

        service.exec(this, event.target, service.register, {
            first_name: this.state.firstNameData,
            last_name: this.state.lastNameData,
            address: this.state.addressData,
            phone: this.state.phoneData,
            email: this.state.emailData,
            password: this.state.passwordData,
        });
    }

    render() {
        if (this.props.isLogged) {
            return <Redirect to={Route.toUrl("user_history")} />
        }

        if (this.state.registered) {
            return <Redirect to={Route.toUrl("auth_verify")} />
        }

        return (
            <div className="site-section bg-light">
                <Container>
                    <Row>
                        <Col lg={6} className="offset-lg-3">
                            <form onSubmit={this.formSubmitRegister} className="p-25 bg-white box-shadowed">
                                <h2 className="mb-4 text-black">Գրանցվել</h2>

                                <FormGroup>
                                    <Row>
                                        <Col md={6}>
                                            <div className="input-area">
                                                <div className="input-content">
                                                    <Label for="first-name">Անուն <sup className="input-required fas fa-asterisk" /></Label>
                                                    <Input type="text" id="first-name" className="rounded-0" autoComplete="off" onChange={this.validateFirstName} />
                                                </div>
                                                <span className="error-message">
                                                    <span className="error-content" />
                                                </span>
                                            </div>
                                        </Col>
                                        <Col md={6}>
                                            <div className="input-area">
                                                <div className="input-content">
                                                    <Label for="last-name">Ազգանուն <sup className="input-required fas fa-asterisk" /></Label>
                                                    <Input type="text" id="last-name" className="rounded-0" autoComplete="off" onChange={this.validateLastName} />
                                                </div>
                                                <span className="error-message">
                                                    <span className="error-content" />
                                                </span>
                                            </div>
                                        </Col>
                                    </Row>
                                </FormGroup>

                                <FormGroup>
                                    <Row>
                                        <Col md={6}>
                                            <div className="input-area">
                                                <div className="input-content">
                                                    <Label for="address">Բնակության հասցե <sup className="input-required fas fa-asterisk" /></Label>
                                                    <Input type="address" id="address" className="rounded-0" autoComplete="off" onChange={this.validateAddress} />
                                                </div>
                                                <span className="error-message">
                                                    <span className="error-content" />
                                                </span>
                                            </div>
                                        </Col>
                                        <Col md={6}>
                                            <div className="input-area">
                                                <div className="input-content">
                                                    <Label for="phone">Բջջային համար <sup className="input-required fas fa-asterisk" /></Label>
                                                    <InputMask mask="+37499999999" value={this.state.phoneData} onChange={this.validatePhone}>{(inputProps) => (
                                                        <Input type="tel" id="phone" className="rounded-0 monospaced" autoComplete="off" {...inputProps} />
                                                    )}</InputMask>
                                                </div>
                                                <span className="error-message">
                                                    <span className="error-content" />
                                                </span>
                                            </div>
                                        </Col>
                                    </Row>
                                </FormGroup>

                                <FormGroup>
                                    <div className="input-area">
                                        <div className="input-content">
                                            <Label for="email">Էլ-փոստ</Label>
                                            <Input type="email" id="email" className="rounded-0" autoComplete="off" onChange={this.validateEmail} />
                                        </div>
                                        <span className="error-message">
                                            <span className="error-content" />
                                        </span>
                                    </div>
                                </FormGroup>

                                <FormGroup>
                                    <Row>
                                        <Col md={6}>
                                            <div className="input-area">
                                                <div className="input-content">
                                                    <Label for="password">Գաղտնաբառ <sup className="input-required fas fa-asterisk" /></Label>
                                                    <Input type="password" id="password" className="rounded-0" onChange={this.validatePassword} />
                                                </div>
                                                <span className="error-message">
                                                    <span className="error-content" />
                                                </span>
                                            </div>
                                        </Col>
                                        <Col md={6}>
                                            <div className="input-area">
                                                <div className="input-content">
                                                    <Label for="confirm">Հաստատել գաղտնաբառը <sup className="input-required fas fa-asterisk" /></Label>
                                                    <Input type="password" id="confirm" className="rounded-0" onChange={this.validateConfirm} />
                                                </div>
                                                <span className="error-message">
                                                    <span className="error-content" />
                                                </span>
                                            </div>
                                        </Col>
                                    </Row>
                                </FormGroup>

                                <FormGroup>
                                    <Button color="success" className="w-100 py-2 px-4 rounded-0">Գրանցվել</Button>
                                    <ul className="auth-error-message" id="auth-error-message" />
                                    <ul className="auth-success-message" id="auth-success-message" />
                                </FormGroup>

                                <div className="loading-area" id="loading-area" />
                                <div className="link-area">
                                    <Link to={Route.toUrl("auth_login")}>Արդեն գրանցված ե՞ք:</Link>
                                    <br />
                                    <Link to={Route.toUrl("auth_forgot_password")}>Մոռացել ե՞ք գաղտնաբառը:</Link>
                                </div>
                            </form>
                        </Col>
                    </Row>
                </Container>
            </div>
        );
    }
}

Register.propTypes = {
    dispatch: PropTypes.func.isRequired,
    isLogged: PropTypes.bool.isRequired,
};

export default connect((state) => {
    return {
        isLogged: state.AuthReducer.isLogged,
    };
})(Register);
