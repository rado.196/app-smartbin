
export default new class {
    constructor() {
        this._routeRedirect = [];
        this._routeList = [];
        this._routeLink = {};

        this._prefix = "";
    }

    group(prefix, callback) {
        const oldPrefix = this._prefix;
        this._prefix += prefix;

        callback();
        this._prefix = oldPrefix;
    }

    add(name, routeItem) {
        let route = routeItem;
        route.path = this._prefix + route.path;
        route.exact = true;

        this._routeList.push(route);
        this._routeLink[name] = route.path;
    }

    index(component, layout) {
        this.add("index", {
            path: "/",
            component,
            layout,
        });
    }

    error(component, layout) {
        this.add("error", {
            path: "*",
            component,
            layout,
        });
    }

    redirectTo(from, to) {
        this._routeRedirect.push({
            from,
            to
        });
    }

    redirectRoute(from, name, args = {}) {
        this.redirectTo(from, this.toUrl(name, args));
    }

    toUrl(name, args = {}) {
        let path = this._routeLink[name];
        for (let key in args) {
            const re = new RegExp(`\\:${key}`, "g");
            path = path.replace(re, args[key]);
        }

        return path;
    }

    info(name) {
        return this.routeList[name];
    }

    redirectList() {
        return this._routeRedirect;
    }

    routeList() {
        return this._routeList;
    }
}
