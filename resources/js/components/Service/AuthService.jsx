import axios from "axios";
import reactTriggerChange from "react-trigger-change";

import * as actions from "../Store/actions";

const URL_PREFIX = "/api/auth";

const showMessages = (type, messages, form) => {
    let html = "";
    Object.keys(messages).map((key) => {
        let message = messages[key];
        html += `<li>${message}</li>`;
    });

    if (undefined === form || null === form) {
        form = document.querySelector("form");
    }

    let messageSpan = form.querySelector(`.auth-${type}-message`);
    messageSpan.innerHTML = html;

    setTimeout(() => {
        messageSpan.innerHTML = "";
    }, 5000);
};

const errorCatch = (reject, err) => {
    let errorsResult = {};
    switch (err.response.status) {
        case 422:
            let errors = err.response.data.errors;
            for (let key in errors) {
                errorsResult[key] = errors[key][0];
            }

            showMessages("error", errorsResult);
            break;

        case 401:
        case 403:
            showMessages("error", {
                global: err.response.data.message
            });
            break;
    }

    return reject(err);
};

// ----------------------------------------------------------------------------------------------
// --- show message

export const showError = (message) => {
    showMessages("error", [
        message
    ]);
};

export const showSuccess = (message) => {
    showMessages("success", [
        message
    ]);
};

// ----------------------------------------------------------------------------------------------
// --- animate

export const exec = (component, form, actionCall, data = {}) => {
    const loading = (null !== form ? form : document).querySelector(".loading-area");
    const stopAnimation = () => {
        if (loading) {
            loading.classList.remove("loading-show");
        }
    };

    loading.classList.add("loading-show");

    const actionResult = actionCall(data, component, form);
    return component.props.dispatch(actionResult)
        .then(stopAnimation)
        .catch(stopAnimation);
};

// ----------------------------------------------------------------------------------------------
// --- validate

const validateContent = (valid, element, error) => {
    if (valid) {
        element.parentNode.parentNode.classList.remove("invalid");
        element.parentNode.nextSibling.innerText = "";

        return true;
    } else {
        element.parentNode.parentNode.classList.add("invalid");
        element.parentNode.nextSibling.innerText = error;

        return false;
    }
};

export const validFirstName = (element) => {
    return validateContent(
        "" !== element.value,
        element,
        "Անուն դաշտը պարտադիր է:",
    );
};

export const validLastName = (element) => {
    return validateContent(
        "" !== element.value,
        element,
        "Ազգանուն դաշտը պարտադիր է:",
    );
};

export const validAddress = (element) => {
    return validateContent(
        "" != element.value,
        element,
        "Սխալ բնակության հասցե:",
    );
};

export const validEmail = (element) => {
    let reEmail = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    
    const val = element.value.toLowerCase();
    return validateContent(
        "" === val || reEmail.test(val),
        element,
        "Էլփոստի հասցեն սխալ է:",
    );
};

export const validPhone = (element) => {
    let rePhone = /^\+374(41|43|44|55|77|91|93|94|95|96|98|99)[\d]{6}$/;
    
    const val = element.value.toLowerCase();
    return validateContent(
        rePhone.test(val),
        element,
        "Անվավեր հեռախոսահամար:",
    );
};

const validPhoneShort = (element) => {
    let rePhone = /^0?(41|43|44|55|77|91|93|94|95|96|98|99)[\d]{6}$/;

    const val = element.value.toLowerCase();
    return validateContent(
        rePhone.test(val),
        element,
        "Անվավեր հեռախոսահամար:",
    );
};

export const validEmailOrPhone = (element) => {
    return validateContent(
        validEmail(element) || validPhone(element) || validPhoneShort(element),
        element,
        "Սխալ էլ-փոստ կամ հեռախոսահամար:",
    );
};

export const validPassword = (element) => {
    return validateContent(
        element.value.length >= 6,
        element,
        "Գաղտնաբառը պետք է լինի նվազագույնը վեց նիշ:",
    );
};

export const validConfirm = (element, password) => {
    return validateContent(
        element.value === password,
        element,
        "Գաղտնաբառերը չեն համապատասխանում:",
    );
};

export const showAllErrors = (form) => {
    const elements = form.querySelectorAll("input, textarea, select");
    elements.forEach(reactTriggerChange);
};

// ----------------------------------------------------------------------------------------------
// --- actions

export const userProvider = new class {
    set(value) {
        localStorage.setItem("user_provider", value);
    }

    unset() {
        localStorage.removeItem("user_provider");
    }

    get() {
        return localStorage.getItem("user_provider");
    }

    is(value) {
        return this.get() === value;
    }
};

const doAction = (callback) => {
    return (dispatch) => new Promise((resolve, reject) => {
        let promise = callback(resolve, reject, dispatch);
        promise.catch((err) => errorCatch(reject, err));
    });
};

export const register = (postData, component, form) => {
    return doAction((resolve, reject, dispatch) => {
        return axios.post(`${URL_PREFIX}/register`, postData)
            .then((response) => {
                if (response.data.status === "success") {
                    component.setState({
                        registered: true,
                    });

                    userProvider.set(response.data.provider);
                    return resolve();
                } else {
                    showMessages("error", response.data, form);
                    return reject();
                }
            });
    });
};

export const verify = (postData, component, form) => {
    return doAction((resolve, reject, dispatch) => {
        return axios.post(`${URL_PREFIX}/verify`, postData)
            .then((response) => {
                if (response.data.status === "success") {
                    component.setState({
                        verified: true,
                    });

                    userProvider.unset();
                    dispatch({
                        type: actions.AUTH_LOGIN,
                        payload: response.data
                    });

                    return resolve();
                } else {
                    showMessages("error", response.data, form);
                    return reject();
                }
            });
    });
};

export const login = (postData, component, form) => {
    return doAction((resolve, reject, dispatch) => {
        return axios.post(`${URL_PREFIX}/login`, postData)
            .then((response) => {
                if (response.data.status === "success") {
                    dispatch({
                        type: actions.AUTH_LOGIN,
                        payload: response.data
                    });

                    return resolve();
                } else if (response.data.status === "not_verified") {
                    component.setState({
                        notVerified: true,
                    });
                } else {
                    showMessages("error", response.data, form);
                    return reject();
                }
            });
    });
};

export const logout = (postData, component, form) => {
    return doAction((resolve, reject, dispatch) => {
        return axios.post(`${URL_PREFIX}/logout`)
            .then((response) => {
                dispatch({
                    type: actions.AUTH_LOGOUT
                });

                return resolve();
            });
    });
};

export const forgotPassword = (postData, component, form) => {
    return doAction((resolve, reject, dispatch) => {
        return axios.post(`${URL_PREFIX}/password/reset/email`, postData)
            .then((response) => {
                if (response.data.status === "success") {
                    component.setState({
                        codeSent: true,
                    });

                    userProvider.set(response.data.provider);
                    return resolve();
                } else {
                    showMessages("error", response.data, form);
                    return reject();
                }
            });
    });
};

export const verifyReset = (postData, component, form) => {
    return doAction((resolve, reject, dispatch) => {
        return axios.post(`${URL_PREFIX}/password/reset/verify`, postData)
            .then((response) => {
                if (response.data.status === "success") {
                    component.setState({
                        validCode: true,
                    });

                    return resolve();
                } else {
                    showMessages("error", response.data, form);
                    return reject();
                }
            });
    });
};

export const resetValidate = (postData, component, form) => {
    return doAction((resolve, reject, dispatch) => {
        return axios.get(`${URL_PREFIX}/password/reset/find`, { params: postData })
            .then((response) => {
                if (response.data.status === "success") {
                    component.setState({
                        status: "success"
                    });

                    return resolve();
                } else {
                    component.setState({
                        status: "error",
                        error: response.data.message
                    });

                    return reject();
                }
            });
    });
};

export const resetPassword = (postData, component, form) => {
    return doAction((resolve, reject, dispatch) => {
        return axios.post(`${URL_PREFIX}/password/reset/reset`, postData)
            .then((response) => {
                if (response.data.status === "success") {
                    component.setState({
                        resetComplete: true,
                    });

                    userProvider.unset();
                    return resolve();
                } else {
                    showMessages("error", response.data, form);
                    return reject();
                }
            });
    });
};

export const changePassword = (postData, component, form) => {
    return doAction((resolve, reject, dispatch) => {
        return axios.post(`${URL_PREFIX}/update/password`, postData)
            .then((response) => {
                if (response.data.status === "success") {
                    component.setState({
                        changedStatus: true
                    });

                    return resolve();
                } else {
                    showMessages("error", response.data, form);
                    return reject();
                }
            });
    });
};

export const update = (postData, component, form) => {
    return doAction((resolve, reject, dispatch) => {
        return axios.post(`${URL_PREFIX}/update/profile`, postData)
            .then((response) => {
                if (response.data.status === "success") {
                    dispatch({
                        type: actions.AUTH_UPDATE,
                        payload: {
                            user: {
                                first_name: postData.first_name,
                                last_name: postData.last_name,
                                email: postData.email,
                                phone: postData.phone,
                            },
                        },
                    });

                    return resolve();
                } else {
                    showMessages("error", response.data, form);
                    return reject();
                }
            });
    });
};
