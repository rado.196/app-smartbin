import React, { Component } from "react";
import { BrowserRouter, Switch, Redirect } from "react-router-dom";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import axios from "axios";

import Store from "./Store";
import * as actions from "./Store/actions";
import AppRoute from "./Routing/Item";
import Routes from "./Routes";

class App extends Component {
    constructor(props, context) {
        super(props, context);

        this.state = {
            loading: true,
            loaded: false,
        };

        const axiosResolve = (response) => {
            if (response.data.auth_remove_token) {
                delete response.data.auth_remove_token;

                Store.dispatch({
                    type: actions.AUTH_LOGOUT,
                });
            }

            return response;
        };

        const axiosReject = (error) => {
            if (error.response.status === 401) {
                Store.dispatch({
                    type: actions.AUTH_LOGOUT,
                });
            }

            return Promise.reject(error);
        };

        axios.defaults.headers.common["X-Requested-With"] = "XMLHttpRequest";
        axios.interceptors.response.use(axiosResolve, axiosReject);

        Store.dispatch({
            type: actions.AUTH_CHECK,
        });

        axios.post("/api/auth/logged-user")
            .then((response) => {
                if (null == response.data.auth_logged_user) {
                    return;
                }

                Store.dispatch({
                    type: actions.AUTH_UPDATE,
                    payload: {
                        user: response.data.auth_logged_user,
                    }
                });
            });
    }

    componentDidMount() {
        document.getElementById("noscript").remove();
        window.addEventListener("load", () => {
            this.setState({
                loading: false,
            });

            setTimeout(() => {
                this.setState({
                    loaded: true,
                });
            }, 300);
        });
    }

    render() {
        return (
            <div>
                {!this.state.loaded && (
                    <div className={`load ${this.state.loading ? "" : "loaded"}`}>
                        <div className="load-icon-wrap" />
                    </div>
                )}

                {!this.state.loading && <BrowserRouter>
                    <Switch>
                        {Routes.redirectList().map((redirect, i) => (
                            <Redirect key={i} {...redirect} />
                        ))}

                        {Routes.routeList().map((route, i) => (
                            <AppRoute key={i} {...route} />
                        ))}
                    </Switch>
                </BrowserRouter>}
            </div>
        );
    }
}

App.propTypes = {

};

export default connect((state) => {
    return {};
})(App);
