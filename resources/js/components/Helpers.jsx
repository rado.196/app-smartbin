import JavascriptTimeAgo from "javascript-time-ago";
import hy from "javascript-time-ago/locale/hy";

JavascriptTimeAgo.locale(hy);
const timeAgo = new JavascriptTimeAgo("hy-AM");

export default class Helpers {
    static humanizeDateDifference(date) {
        return timeAgo.format(new Date(date));
    }

    static formatDateTime(date) {
        const d = new Date(date);
        let day = d.getDate();
        let month = d.getMonth() + 1;
        let year = d.getFullYear();
        let hour = d.getHours();
        let minute = d.getMinutes();

        if (day < 10) { day = "0" + day; }
        if (month < 10) { month = "0" + month; }
        if (hour < 10) { hour = "0" + hour; }
        if (minute < 10) { minute = "0" + minute; }

        return `${day}.${month}.${year} ${hour}:${minute}`;
    }

    static formatDate(date) {
        const d = new Date(date);
        let day = d.getDate();
        let month = d.getMonth() + 1;
        let year = d.getFullYear();

        if (day < 10) { day = "0" + day; }
        if (month < 10) { month = "0" + month; }

        return `${month}/${day}/${year}`;
    }

    static formatNumber(number) {
        const reverse = function (str) {
            return str.split("").reverse().join("");
        };

        let str = number.toString();
        str = reverse(str);

        str = str.match(/.{1,3}/g);
        str = str.join("'");

        return reverse(str);
    }

    static pageTitle(title) {
        document.title = `SmartBin - ${title}`;
    }
}
