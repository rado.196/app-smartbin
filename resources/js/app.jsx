import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";

import Store from "./components/Store";
import App from "./components/App";

ReactDOM.render(
    <Provider store={Store}>
        <App />
    </Provider>,
    document.getElementById("app-root")
);
