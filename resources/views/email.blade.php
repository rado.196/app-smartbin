<!doctype html>
<html lang="hy-AM">
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <title>{{ env('APP_NAME') }} &mdash; @yield('title')</title>
</head>
<body>

    {{ $body }}

</body>
</html>